FROM python:3.8-slim

# Set work directory
WORKDIR /code

ENV PYTHONUNBUFFERED 1

RUN apt-get update -y
RUN apt-get install git -y
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/
CMD python3 main.py
